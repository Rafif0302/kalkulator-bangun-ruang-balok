package com.example.uh1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class activityUH extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uh);

        Intent intent = getIntent();

        String result = intent.getStringExtra("data");
        TextView tv = findViewById(R.id.tvResult);
        tv.setText(result);

        String a = intent.getStringExtra("nama");
        TextView nam = findViewById(R.id.nama);
        nam.setText(a);
    }
}
