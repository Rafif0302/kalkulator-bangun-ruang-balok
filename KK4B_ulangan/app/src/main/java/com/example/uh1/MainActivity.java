package com.example.uh1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText hit_panjang, hit_lebar, hit_tinggi;
    private double p, l, t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hit_panjang = findViewById(R.id.panjang);
        hit_lebar = findViewById(R.id.lebar);
        hit_tinggi = findViewById(R.id.tinggi);
    }

    private void data(List<String> variabel) {
        String panjang = hit_panjang.getText().toString();
        String lebar = hit_lebar.getText().toString();
        String tinggi = hit_tinggi.getText().toString();

        p = Double.parseDouble(panjang);
        l = Double.parseDouble(lebar);
        t = Double.parseDouble(tinggi);
    }

    public void hitung_keliling(View view) {
        if ((hit_panjang.getText().toString().length() == 0) & (hit_lebar.getText().toString().length() == 0) & (hit_tinggi.getText().toString().length() == 0)) {
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        } else if ((hit_panjang.getText().toString().length() == 0) & (hit_lebar.getText().toString().length() == 0) & (hit_tinggi.getText().toString().length() == 0)) {
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        } else if ((hit_panjang.getText().toString().length() == 0) & (hit_lebar.getText().toString().length() == 0) & (hit_tinggi.getText().toString().length() == 0)) {
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        } else if ((hit_panjang.getText().toString().length() == 0) & (hit_lebar.getText().toString().length() == 0)) {
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            return;
        } else if ((hit_panjang.getText().toString().length() == 0) & (hit_tinggi.getText().toString().length() == 0)) {
            hit_panjang.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        } else if ((hit_lebar.getText().toString().length() == 0) & (hit_tinggi.getText().toString().length() == 0)) {
            hit_lebar.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        } else if (hit_panjang.getText().toString().length() == 0) {
            hit_panjang.setError("harap diisi dahulu");
        } else if (hit_lebar.getText().toString().length() == 0) {
            hit_lebar.setError("harap diisi dahulu");
        } else if (hit_tinggi.getText().toString().length() == 0) {
            hit_tinggi.setError("harap diisi dahulu");
        } else {
            List<String> variabel = new ArrayList<>();
            data(variabel);

            Double keliling = 4 * (p + l + t);
            String Nama = "Keliling";
            Intent intent = new Intent(this, activityUH.class);
            intent.putExtra("data", String.valueOf(keliling));
            intent.putExtra("nama", String.valueOf(Nama));

            startActivity(intent);
        }
    }
    public void hitung_luas(View view) {
        if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_lebar.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if (hit_panjang.getText().toString().length()==0){
            hit_panjang.setError("harap diisi dahulu");
        }
        else if (hit_lebar.getText().toString().length()==0){
            hit_lebar.setError("harap diisi dahulu");
        }
        else if (hit_tinggi.getText().toString().length()==0){
            hit_tinggi.setError("harap diisi dahulu");
        }else {

            List<String> variabel = new ArrayList<>();
            data(variabel);
            Double luas = 2 * (p * l + p * t + l * t);
            String Nama = "Luas";
            Intent intent = new Intent(this, activityUH.class);
            intent.putExtra("data", String.valueOf(luas));
            intent.putExtra("nama", String.valueOf(Nama));

            startActivity(intent);
        }
    }
    public void hitung_volume(View view) {

        if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_lebar.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_lebar.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_panjang.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_panjang.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if((hit_lebar.getText().toString().length()==0)&(hit_tinggi.getText().toString().length()==0)){
            hit_lebar.setError("harap diisi dahulu");
            hit_tinggi.setError("Harap diisi dahulu");
            return;
        }
        else if (hit_panjang.getText().toString().length()==0){
            hit_panjang.setError("harap diisi dahulu");
        }
        else if (hit_lebar.getText().toString().length()==0){
            hit_lebar.setError("harap diisi dahulu");
        }
        else if (hit_tinggi.getText().toString().length()==0){
            hit_tinggi.setError("harap diisi dahulu");
        }else {
            List<String> variabel = new ArrayList<>();
            data(variabel);
            Double volume = p * l * t;
            String Nama = "Volume";
            Intent intent = new Intent(this, activityUH.class);
            intent.putExtra("data", String.valueOf(volume));
            intent.putExtra("nama", String.valueOf(Nama));
            startActivity(intent);
        }
    }
}
